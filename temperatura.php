<?php 
	require('server/selectRoom.php'); ?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<!-- <meta http-equiv="refresh" content="60; URL=http://www.gmarsi.com/ambientduino/temperatura.php?room=<?php echo $roomName; ?>"> -->
		<title>Temperatura</title>
		<link rel="stylesheet" href="styles/style.css">
	</head>
	<body>
		<input type="text" id="roomName" value="<?php echo $roomName; ?>" style="display: none">
		<input type="text" id="roomTemp" value="<?php echo $arrayTemp[0]; ?>" style="display: none">
		<input type="text" id="roomHum" value="<?php echo $arrayHum[0]; ?>" style="display: none">
		<div id="container">
			<h2><?php echo $roomName; ?></h2>
			
			<div style="position: relative;">
				<canvas id="chartHabitacion" width="600" height="600"></canvas>
				<div id="data" style = "position: absolute;top: 25%; left: 50%">
					<div style = "position: absolute; width: 140px; top: 290px; height: 30px; left: calc(-50px + 50%); text-align: center;">
						<p id="tempTitle" style="font-size: 200%;font-weight: bold;color: #fff"><?php echo $temperatura . " °C" ?></p>
					</div>
					<div style = "position: absolute;width: 140px; top: 360px; height: 30px; left: calc(-50px + 50%); text-align: center;">
						<p  style="font-size: 200%;font-weight: bold;color: #72abfe"><?php echo $humedad . " %" ?></p>
					</div>
					<div id="tempIcon">
						<img id="tempImage" width="60" src="" alt="Temperature image">
					</div>
				</div>
			</div>
			<p>Última actualicación: <?php echo $dias . " a las " .  $horas ?></p>
		</div>
	</body>
	<script src="scripts/grafico.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.bundle.min.js"></script>
</html>