var temper = ""
var room = document.getElementById('roomName').value
var temps = document.getElementById('roomTemp').value
var hum = document.getElementById('roomHum').value

function checkTemp() {
    let tempColor = ""
    if(temps < 20){
        let tempOkImage = './assets/img/templow.svg'
        tempColor = '#46dbffff'
        document.getElementById('tempTitle').style.color = tempColor
        document.getElementById('tempIcon').style.background = tempColor
        document.getElementById('tempImage').src = tempOkImage
    }else if(temps >= 20 && temps < 25){
        let tempOkImage = './assets/img/tempok.svg'
        tempColor = '#05ff7a'
        document.getElementById('tempTitle').style.color = tempColor
        document.getElementById('tempIcon').style.background = tempColor
        document.getElementById('tempImage').src = tempOkImage
    }else{
        tempOkImage = './assets/img/temphigh.svg'
        tempColor = '#fd4646ff'
        document.getElementById('tempTitle').style.color = tempColor
        document.getElementById('tempIcon').style.background = tempColor
        document.getElementById('tempImage').src = tempOkImage
    }
    crearGrafico(tempColor)

}

    // function startTimer(duration, display) {
    //     var timer = duration, minutes, seconds;
    //     setInterval(function () {
    //         minutes = parseInt(timer / 60, 10)
    //         seconds = parseInt(timer % 60, 10);
    //         minutes = minutes < 10 ? "0" + minutes : minutes;
    //         seconds = seconds < 10 ? "0" + seconds : seconds;
    //         display.textContent = minutes + ":" + seconds;
    //         if (--timer < 0) {
    //         timer = duration;
    //         }
    //     }, 1000);
    // }
    window.onload = function () {
        // Comprobamos la temperatura
        checkTemp()
        // var fiveMinutes = 60,
        // display = document.querySelector('#time');
        // startTimer(fiveMinutes, display);
    };
    function crearGrafico(tempColor){
        var ctx = document.getElementById('chartHabitacion').getContext('2d');
        var temp = 24
        var tempAux = 40 - temp
        var hum = 70
        var humAux = 100 - hum
        var myChart = new Chart(ctx, {
            type: 'doughnut',
            data:{
                    datasets: [
                        {
                            label: 'Humedad',
                            data: [hum, humAux],
                            backgroundColor: ['#0f71fd96', '#fff'],
                            borderColor: ['rgba(132,99,255,1)', '#e3e3e3'],
                            borderWidth: 0
                        },
                        {
                            label: 'Temperatura',
                            data: [temp, tempAux],
                            backgroundColor: [tempColor, '#fff'],
                            borderColor: ['rgba(255,99,132,1)', '#e3e3e3'],
                            borderWidth: 0
                        },
                    ]
            },
            options: {
                    responsive: true,
                    rotation: 2.2,
                    cutoutPercentage: 50,
                    circumference: 5,
                    scales: 
                        {
                            display: false,
                                yAxes: [
                                    { display: false }
                                ],
                                xAxes: [
                                    { display: false }
                                ],
                        },
                            legend:{
                                /* display: false, */
                            },
                            tooltips: { enabled: false },
                            hover:{ mode: null  }
            }
        });
    }